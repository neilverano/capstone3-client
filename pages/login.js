import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Router from 'next/router'
import Swal from 'sweetalert2'
import {GoogleLogin} from 'react-google-login'

export default function Login(){

	const {setUser} = useContext(UserContext)
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')

	function authenticate(e){
		e.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/login`, {

			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data.accessToken){
				localStorage.setItem('token', data.accessToken)
				fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					console.log(data)
					setUser({
						id: data._id,
						email: data.email,
						isAdmin: data.isAdmin
					})
					Swal.fire({
						icon: "success",
						title: "Successful Login"
					})
					Router.push('/')
				})
			} else {
				Swal.fire({
					icon: "error",
					title: "Authentication Failed."
				})
			}
		})

	}

const authenticateGoogleToken = (response) => {

	const payload = {

		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			tokenId: response.tokenId,
			googleToken: response.accessToken
		})

	}

	fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/verify-google-id-token`, payload)
	.then(res => {
		return res.json()
	})
	.then(data => {
		if(typeof data.access !== 'undefined'){
			console.log('User has a valid token')
			localStorage.setItem('token', data.access)
			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
				headers: {
					'Authorization': `Bearer ${data.access}`
				}
			})
			.then(res => {
				return res.json()
			})	
			.then(data => {
				setUser({
					id: data._id,
					email: data.email,
				})
			})
		} else {
			if(data.error === 'google-auth-error'){
				Swal.fire({
					icon: 'error',
					title: 'Google Authentication Error.',
				})
			} else if (data.error === 'login-type-error'){
				Swal.fire({
					icon: 'error',
					title: 'Login Type Error'
				})
			}
		}
	})

}




	return(

		<Form onSubmit={(e) => authenticate(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address:</Form.Label>
				<Form.Control type='email' placeholder='Enter Email Address' value={email} onChange={(e) => setEmail(e.target.value)} required />
			</Form.Group>
			<Form.Group controlId="userPassword">
				<Form.Label>Password:</Form.Label>
				<Form.Control type='password' placeholder='Enter Password' value={password} onChange={(e) => setPassword(e.target.value)} required />
			</Form.Group>
			<Button variant="primary" type="submit">Submit</Button>
			<GoogleLogin
				clientId='384737055557-b4sheuqnqtv9g9e143bna6douisiaafb.apps.googleusercontent.com'
				buttontext='Login using Google'
				cookiePolicy={'single_host_origin'}
				onSuccess={authenticateGoogleToken}
				onFailure={authenticateGoogleToken}
				className='w-100 text-center d-flex justify-content-center'
			/>
		</Form>		
		)


}