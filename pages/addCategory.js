import { useState, useEffect } from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Swal from 'sweetalert2'
import Router from 'next/router'

export default function addCategory(){
	const [ name, setName ] = useState('')
	const [ type, setType ] = useState('')
	const [ isActive, setIsActive ] = useState(false)

	function newCat(e){
		e.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/addCategory`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({	
					categoryType: type,
					categoryName: name								
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					icon: 'success',
					title: 'Successfully Created New Category'
				})
				setName('')
				setType('income')
				Router.push('/categories')
			}else{
				Swal.fire({
					icon: 'error',
					title: 'Something is Wrong'
				})
			}
		})			
	}

	useEffect(()=>{
		if(name !== '' && type !== 'select' && type !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	})


	return(
		<>
			<h2>Add Category</h2>
			<Form onSubmit={(e) => newCat(e)}>				
				<Form.Group controlId='name'>
					<Form.Label>Category Name:</Form.Label>
					<Form.Control type='text' placeholder='Enter Category Name' value={name} onChange={(e) => setName(e.target.value)} required/>	
				</Form.Group>
				<Form.Group controlId='type'>
					<Form.Label>Category Type:</Form.Label>
					<Form.Control as='select' value={type} onChange={(e) => setType(e.target.value)} required>
						<option value={"select"}>Select</option>
						<option value={"income"}>Income</option>
						<option value={"expenses"}>Expense</option>
					</Form.Control>	
				</Form.Group>
				{
					isActive
					?	<Button variant='primary' type='submit' block>Add</Button>
					:	<Button variant='primary' type='submit' block disabled>Add</Button>
				}
			</Form>
		</>
	)
}